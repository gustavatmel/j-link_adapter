View this project on [CADLAB.io](https://cadlab.io/project/1770). 

j-link_adapter

---
# TODO

## Schematic 
- [x] STM stecker
- [ ] USB-C ?

## PCB
### j-link_adapter
- [ ] Powerline wider
- [ ] better marking with tool from https://github.com/gregdavill/KiBuzzard

### edgeCom_adapter
- [ ] round corners
